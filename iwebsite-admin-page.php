<?php

if ( ! class_exists( 'iWebsite_Admin_Page' ) ) {
	class iWebsite_Admin_Page {

		public $path_to_files;

		public $admin_settings_url;

		public $discount_settings;

		public $discount_pages;

		public $localization_domain;

		public function __construct() {

			$this->path_to_files      = IWEBSITE_ADMIN_MENU_PATH;
			$this->admin_settings_url = 'iwebsite-admin-menu-settings';

			$this->localization_domain = CHILD_THEME_NAME;

			add_action( 'admin_menu', array ( $this, 'register_settings_page' ), 1 );
			add_action( 'admin_enqueue_scripts', array ( $this, 'admin_iwebsite_admin_menu_script' ), 50 );

		}

		/**
		 * Method than enqueue scripts and styles for admin
		 */
		public function admin_iwebsite_admin_menu_script() {
			if ( isset( $_GET[ 'page' ] ) ) {
				wp_enqueue_media();
				wp_enqueue_script( 'jquery-ui-core' );
				wp_enqueue_script( 'jquery-ui-accordion' );
				wp_enqueue_script( 'jquery-ui-tooltip' );

				wp_enqueue_style( 'jquery-ui', IWEBSITE_ADMIN_MENU_URI . '/admin/style/jquery-ui.min.css' );
				wp_enqueue_style( 'admin-menu-style', IWEBSITE_ADMIN_MENU_URI . '/admin/style/style.css' );
				wp_enqueue_style( 'admin-menu-swal', IWEBSITE_ADMIN_MENU_URI . '/admin/sweetalert/sweetalert.css' );
				wp_enqueue_script( 'admin-menu-script', IWEBSITE_ADMIN_MENU_URI . '/admin/js/script.js', array (
					'jquery',
					'jquery-ui-accordion'
				), '', true );
				wp_enqueue_script( 'admin-menu-swal', IWEBSITE_ADMIN_MENU_URI . '/admin/sweetalert/sweetalert.min.js', array (), '', true );
			}
		}

		/**
		 * Register the menu item and its sub menu's.
		 *
		 * @global array $submenu used to change the label on the first item.
		 */
		public function register_settings_page() {
			$icon_svg           = '';
			$manage_options_cap = 'manage_options';
			// Add main page.
			$admin_page = add_menu_page( 'Admin Menu settings', 'iWebsite Admin Menu ', $manage_options_cap, $this->admin_settings_url, array (
				$this,
				'load_page'
			), $icon_svg, '50' );
		}

		public function load_page() {
			$userrole_menu_class = new iWebsite_Menu_For_Userroles();

			$this->iwebsite_admin_menu_update_message( $userrole_menu_class );
			$this->iwebsite_admin_menu_delete_message($userrole_menu_class );

			?>
			<div class="wrap">
				<div class="sale">


					<form action="#" method="post" id="discount-settings-form">
						<div class="sale-container">
							<div class="sale-container-inner">
								<?php $userrole_menu_class->admin_page_settings(); ?>
							</div>
						</div>
						<div class="submit">
							<input type="submit" name="admin_menu_settings" value="<?php _e( 'Save settings', $userrole_menu_class->localization_domain ); ?>" class="button button-primary admin-menu-submit-settings">
							<?php wp_nonce_field( 'admin-menu-settings-update' ); ?>
						</div>
					</form>
					<form action="#" method="post" >
						<h2><?php _e('Remove all settings',$userrole_menu_class->localization_domain) ?> </h2>
						<input type="hidden" name="remove_all_settings" value="true">
						<?php wp_nonce_field( 'admin-menu-settings-delete' ); ?>
						<input type="submit" value="<?php _e('Remove',$userrole_menu_class->localization_domain) ?>" >
					</form>
				</div>
			</div>
			<?php

			// === FOR ADMIN SETTINGS === //

		}


		private function iwebsite_admin_menu_delete_message(  $userrole_menu_class) {
			if ( isset( $_POST[ '_wpnonce' ] ) && !empty( $_POST[ '_wpnonce' ] ) ){
				if ( wp_verify_nonce( $_POST[ '_wpnonce' ], 'admin-menu-settings-delete' ) ) {
					delete_option(  $userrole_menu_class->option_name );
				}
			}

		}
		private function iwebsite_admin_menu_update_message( $admin_menu_class ) {
			$message = null;
			if ( isset( $_POST[ 'admin_menu_settings' ] ) && $_POST[ 'admin_menu_settings' ] ) {
				if ( $this->iwebsite_admin_menu_settings_submission( $admin_menu_class ) ) {
					$message = '<div class="updated"><p>' . __( 'Success! Your changes were sucessfully saved!', $this->localization_domain ) . '</p></div>';
				} else {
					$message = '<div class="error"><p>' . __( 'Error. Either you did not make any changes, or your changes did not save. Please try again.', $this->localization_domain ) . '</p></div>';
				}
			}

			return $message;
		}

		// Saving user settings, work with POST variable, updating variables with settings
		public function iwebsite_admin_menu_settings_submission( $admin_menu_class ) {
			if ( ! wp_verify_nonce( $_POST[ '_wpnonce' ], 'admin-menu-settings-update' ) ) {
				die( __( 'Whoops! There was a problem with the data you posted. Please go back and try again.', $this->localization_domain ) );
			}

			if ( isset( $_POST ) ) {
				$ar_temp_settings      = array ();
				$current_discount_type = $admin_menu_class->option_name;
				$unserialized_array    = array ();

				foreach ( $_POST as $post_key => $post_value ) {
					if ( is_array( $post_value ) ) {

						foreach ( $post_value as $key => $val ) {
							if ( $post_key != 'userrole'&& $post_key != 'username') {
								$post_key_array = substr( $post_key, 0, strripos( $post_key, '_' ) );
								$post_key_num   = substr( $post_key, strripos( $post_key, '_' ) + 1 );
								$post_key_num   = (integer) $post_key_num;

								if ( isset( $ar_temp_settings[ $key ][ 'serialized_menu_array' ] ) && ! empty( $ar_temp_settings[ $key ][ 'serialized_menu_array' ] ) ) {
									$serialized_array_pre                                       = $ar_temp_settings[ $key ][ 'serialized_menu_array' ];
									$unserialized_array_pre                                     = unserialize( $serialized_array_pre );
									$unserialized_array_pre[ $post_key_num ][ $post_key_array ] = $val;
									$serialized_array_pre                                       = serialize( $unserialized_array_pre );
									$ar_temp_settings[ $key ][ 'serialized_menu_array' ]        = $serialized_array_pre;
								} else {
									$unserialized_array_pre                                     = array ();
									$unserialized_array_pre[ $post_key_num ][ $post_key_array ] = $val;
									$serialized_array_pre                                       = serialize( $unserialized_array_pre );
									$ar_temp_settings[ $key ][ 'serialized_menu_array' ]        = $serialized_array_pre;
								}

								continue;
							}

							if ( $val == '' ) {
								$val = false;
							}
							$ar_temp_settings[ $key ][ $post_key ] = $val;
						}

					} else {

						if ( ! intval( $post_key ) && $current_discount_type != 'iwebsite_all_products' ) {
							continue;
						}
						if ( $post_value == '' ) {
							$post_value = false;
						}

						$ar_temp_settings[ $post_key ] = $post_value;
					}

				}

				// pass saved settings to class to update in on a page
				$admin_menu_class->set_admin_menu_settings( $ar_temp_settings );
				$admin_menu_class->change_admin_menu_label();
				update_option( $current_discount_type, $ar_temp_settings );

				if ( isset( $_GET[ 'delete_option' ] ) ) {
					delete_option( $current_discount_type );
				}
			}

			return false;
		}
	}
}

