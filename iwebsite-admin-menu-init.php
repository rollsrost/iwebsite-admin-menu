<?php

/*
Plugin Name: iWebsite-admin-menu
Plugin URI: http://iwebsite.co.il
Description: Plugin for change admin menu
Version: 1.0.0
Author: iWebsite
Author URI:  http://iwebsite.co.il
Text Domain: iwebsite-admin-plugin
Domain Path: /languages
*/


define( 'IWEBSITE_ADMIN_MENU_PATH', dirname( __FILE__ ) );
define( 'IWEBSITE_ADMIN_MENU_URI', plugin_dir_url( __FILE__ ) );

if ( ! class_exists( 'iWebsite_Admin_Menu_Init' ) ) {
	class iWebsite_Admin_Menu_Init {
		public $ar_active_sales;

		//Basic settings for admin
		public function __construct() {
			require_once( IWEBSITE_ADMIN_MENU_PATH . '/iwebsite-admin-page.php' );
			add_action( 'wp_loaded' , array ( $this, 'sale_init' ) );
			add_action( 'plugins_loaded' , array($this, 'localize_iwebsite_admin_menu') );

		}
		public function localize_iwebsite_admin_menu() {
			load_plugin_textdomain('iwebsite-adminmenu', false, dirname( plugin_basename(__FILE__) ) . '/languages/');
		}
		public function sale_init() {

			require_once( IWEBSITE_ADMIN_MENU_PATH . '/iwebsite-userroles-admin-menu.php' );
			$admin = new iWebsite_Admin_Page();

		}
	}
}

if ( class_exists( 'iWebsite_Admin_Menu_Init' ) ) {
	$class = new iWebsite_Admin_Menu_Init();
}

?>