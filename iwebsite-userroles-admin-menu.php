<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class iWebsite_Menu_For_Userroles {
	public $current_user_roles;
	public $current_username;
	public $settings = array ();
	public $current_settings = array ();
	public $option_name;
	public $localization_domain;
	public $default_settings = array ();

	public function __construct() {

		$this->init_variables();
		add_action( 'admin_menu', array ( $this, 'init_variables' ), 5 );

		add_action( 'admin_menu', array ( $this, 'change_admin_menu_label' ), 999 );

		add_action( 'admin_menu', array ( $this, 'remove_menus' ), 999 );
		add_filter( 'menu_order', array ( $this, 'custom_menu_order' ), 60 );
		add_filter( 'custom_menu_order', array ( $this, 'custom_menu_order' ), 60 );
	}

	/**
	 * Method for removing  menu items
	 */
	function remove_menus() {
		if ( ! $this->check_if_user_have_settings() ) {
			return;
		}
		foreach ( $this->current_settings as $current_setting ) {
			if ( isset( $current_setting[ 'menu_item_hide' ] ) && ! empty( $current_setting[ 'menu_item_hide' ] ) ) {
				if ( $current_setting[ 'menu_item_hide' ] == true ) {
					remove_menu_page( $current_setting[ 'menu_item_name' ] );
				}
			}
		}
	}

	/**
	 * Filter for change  menu items order
	 */
	public function custom_menu_order( $menu_ord ) {
		//		if ( ! $menu_ord ) {
		//			return $menu_ord;
		//		}

		if ( ! $this->check_if_user_have_settings() ) {
			return $menu_ord;
		}
		$priority_array = array ();
		foreach ( $this->current_settings as $current_setting ) {
			if ( isset( $menu_item[ 'menu_item_hide' ] ) && ! empty( $menu_item[ 'menu_item_hide' ] ) && $menu_item[ 'menu_item_hide' ] == true ) {
				continue;
			}

			$priority_array[ $current_setting[ 'menu_item_order' ] ] = $current_setting[ 'menu_item_name' ];
		}

		return $priority_array;
	}

	/**
	 * Method for change label of menu items
	 */
	public function change_admin_menu_label() {

		$wqeqwe = $this->check_if_user_have_settings();

		if ( ! $wqeqwe ) {

			return;
		}
		global $menu;
		foreach ( $menu as $key => $menu_item ) {
			$label          = '';
			$menu_item_slug = $menu_item[ 2 ];
			foreach ( $this->current_settings as $settings_key => $setting ) {
				$needed_key = array_search( $menu_item_slug, $setting );
				if ( $needed_key == false ) {
					continue;
				} else {
					$label = $setting[ 'menu_item_label' ];
				}
				if ( $label != '' ) {
					$menu[ $key ][ 0 ] = $label;
				}
			}
		}
	}

	/**
	 * Method that checks if user match userrole from settings
	 * @return bool
	 */
	public function check_if_user_have_settings() {
		$settings         = $this->settings;
		$current_role     = $this->current_user_roles;
		$current_username = $this->current_username;
		foreach ( $settings as $setting ) {

			$setting_userrole = strtolower( $setting[ 'userrole' ] );
			if ( $setting_userrole == $current_role && ($setting[ 'username' ] == $current_username || $setting[ 'username' ] == 'clear_user_username')) {
				$setting_serialesed     = $setting[ 'serialized_menu_array' ];
				$setting_unserialesed   = unserialize( $setting_serialesed );
				$this->current_settings = $setting_unserialesed;

				return true;
			}
		}

		return false;
	}

	/**
	 * Output admin subpage with settings form
	 */
	public function admin_page_settings() {
		global $wp_roles;
		$wordpress_roles = $wp_roles->get_names();

		$wordpress_users = get_users();

		global $menu;

		foreach ( $this->settings as $setting_key => $setting_value ) {
			$serialized_menu_array = ( isset( $setting_value[ 'serialized_menu_array' ] ) ) ? $setting_value[ 'serialized_menu_array' ] : '';
			$menu_array            = unserialize( $serialized_menu_array );
			$userrole              = ( isset( $setting_value[ 'userrole' ] ) ) ? $setting_value[ 'userrole' ] : '';
			$username              = ( isset( $setting_value[ 'username' ] ) ) ? $setting_value[ 'username' ] : '';
			if ($username=='clear_user_username'){
				$username_label = '';
			}else{
				$username_label = $username;
			}
			?>

			<h3 class="section-title">
				<span class="b"><?php echo sprintf( __( 'Admin menu for  %s %s', $this->localization_domain ), $userrole, $username_label); ?> </span>

			</h3>
			<div id="<?php echo $setting_key ?>" class="sale-repeater">
				<div class="section-discount-content">
					<div class="userrole-username-control">
						<div class="userrole-value">
							<div class="row-title dib">
								<strong><?php _e( 'Select userrole', $this->localization_domain ); ?></strong>
							</div>
							<div class="row-content dib">
								<select class="userrole_select" name="userrole[<?php echo $setting_key; ?>]">
									<?php foreach ( $wordpress_roles as $key => $role ) {
										$selected = ( $userrole == $role ) ? 'selected="selected"' : '';
										echo '<option value="' . $role . '" ' . $selected . ' >' . $role . '</option>';
									} ?>
								</select>
							</div>
						</div>
						<div class="userrole-value username-container">
							<div class="row-title dib">
								<strong><?php _e( 'Select username', $this->localization_domain ); ?></strong>
							</div>
							<div class="row-content dib">
								<select class="username_select" name="username[<?php echo $setting_key; ?>]">
									<option data-userrole="clear_user_username"  value="clear_user_username"  class="clear_user_username" selected="selected" >...</option>

									<?php
									foreach ( $wordpress_users as $key => $role ) {

										$selected = ( $username == $role->user_login ) ? 'selected="selected"' : '';
										echo '<option data-userrole="' . $role->roles[ 0 ] . '" value="' . $role->user_login . '" ' . $selected . ' >' . $role->user_login . '</option>';
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="discount-value">
						<?php

						$not_hidden_array_menu = array ();
						$hidden_array_menu     = array ();

						foreach ( $menu_array as $menu_key => $menu_item ) {
							if ( isset( $menu_item[ 'menu_item_hide' ] ) && ! empty( $menu_item[ 'menu_item_hide' ] ) && $menu_item[ 'menu_item_hide' ] == true ) {
								$hidden_array_menu[ $menu_item[ 'menu_item_order' ] ] = $menu_item;
							} else {
								$not_hidden_array_menu[ $menu_item[ 'menu_item_order' ] ] = $menu_item;
							}
						}
						ksort( $hidden_array_menu );

						ksort( $not_hidden_array_menu );
						$priority_id_count = 1;
						foreach ( $not_hidden_array_menu as $menu_key => $menu_item ) {
							$menu_item_slug  = $menu_item[ 'menu_item_name' ];
							$menu_item_label = $menu_item[ 'menu_item_label' ];
							$menu_item_order = $menu_item[ 'menu_item_order' ];
							if ( isset( $menu_item[ 'menu_item_hide' ] ) && ! empty( $menu_item[ 'menu_item_hide' ] ) ) {
								$menu_item_hide = $menu_item[ 'menu_item_hide' ];
							} else {
								$menu_item_hide = false;
							}
							?>

							<div class="row clearfix border-menu-admin <?php if ( $menu_item_hide ) {
								echo 'grey-color';
							} ?>">
								<div class=" dib one-fourth first">
									<strong class="menu-item-title"><?php echo $menu_item_label; ?></strong>
								</div>
								<label class="one-fourth " for="menu_item_label_<?php echo $menu_key; ?>">
									<span><?php echo __( 'Label', $this->localization_domain ); ?></span>

									<input type="text" name="menu_item_label_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="<?php echo $menu_item_label ?>">
								</label>
								<label class="one-fourth" for="menu_item_label_<?php echo $menu_key; ?>">
									<span><?php echo __( 'Priority', $this->localization_domain ); ?></span>

									<input type="number" data-priority_id="<?php echo $priority_id_count; ?>" class="menu_item_priority priority_id_<?php echo $priority_id_count; ?>" name="menu_item_order_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="<?php echo $menu_item_order ?>">
								</label>

								<label class="one-fourth hide-checkbox">
									<span><?php echo __( 'Hide?', $this->localization_domain ); ?></span>
									<input type="checkbox" name="menu_item_hide_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="true" <?php if ( $menu_item_hide ) {
										echo 'checked="cheked"';
									} ?>>
								</label>
								<input type="hidden" name="menu_item_name_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="<?php echo $menu_item_slug ?>">
							</div>
							<?php

							$priority_id_count ++;
						}
						foreach ( $hidden_array_menu as $menu_key => $menu_item ) {
							$menu_item_slug  = $menu_item[ 'menu_item_name' ];
							$menu_item_label = $menu_item[ 'menu_item_label' ];
							$menu_item_order = $menu_item[ 'menu_item_order' ];
							if ( isset( $menu_item[ 'menu_item_hide' ] ) && ! empty( $menu_item[ 'menu_item_hide' ] ) ) {
								$menu_item_hide = $menu_item[ 'menu_item_hide' ];
							} else {
								$menu_item_hide = false;
							}
							?>

							<div class="row clearfix border-menu-admin <?php if ( $menu_item_hide ) {
								echo 'grey-color';
							} ?>">
								<div class=" dib one-fourth first">
									<strong class="menu-item-title"><?php echo $menu_item_label; ?></strong>
								</div>
								<label class="one-fourth " for="menu_item_label_<?php echo $menu_key; ?>">
									<span><?php echo __( 'Label', $this->localization_domain ); ?></span>
									<input type="text" name="menu_item_label_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="<?php echo $menu_item_label ?>">
								</label>
								<label class="one-fourth" for="menu_item_label_<?php echo $menu_key; ?>">
									<span><?php echo __( 'Priority', $this->localization_domain ); ?></span>

									<input type="number" data-priority_id="<?php echo $priority_id_count; ?>" class="menu_item_priority priority_id_<?php echo $priority_id_count; ?>" name="menu_item_order_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="<?php echo $menu_item_order ?>">
								</label>

								<label class="one-fourth hide-checkbox">
									<span><?php echo __( 'Hide?', $this->localization_domain ); ?></span>

									<input type="checkbox" name="menu_item_hide_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="true" <?php if ( $menu_item_hide ) {
										echo 'checked="cheked"';
									} ?>>
								</label>
								<input type="hidden" name="menu_item_name_<?php echo $menu_key; ?>[<?php echo $setting_key; ?>]" value="<?php echo $menu_item_slug ?>">
							</div>
							<?php

							$priority_id_count ++;

						}
						?>
					</div>
				</div>
				<div class="section-discount-admin">
					<button class="sale-repeater-btn button">
						<span class="dashicons dashicons-plus"></span><span class="label"><?php _e( 'Add row', $this->localization_domain ); ?></span>
					</button>
					<button class="delete-row button">
						<span class="dashicons dashicons-minus"></span><span class="label"><?php _e( 'Delete row from repeater', $this->localization_domain ) ?></span>
					</button>
				</div>
			</div>
			<?php

		}
	}

	/**
	 * Get menu settings for current tab in admin page
	 * @return array
	 */
	public function get_options() {
		if ( ! $options = get_option( $this->option_name ) ) {
			$options[] = $this->default_settings;
			update_option( $this->option_name, $options );
		}

		foreach ( $options as $key => $option ) {
			$options[ $key ] = array_merge( $this->default_settings, $option );
		}
		$this->settings = $options;

		return $options;
	}

	/**
	 * Setter for options
	 *
	 * @param $new_settings
	 *
	 * @return array
	 */
	public function set_admin_menu_settings( $new_settings ) {
		$this->settings = $new_settings;

		return $this->settings;
	}

	/**
	 * Initialize properties of class
	 */
	public function init_variables() {
		global $menu;

		$current_user = wp_get_current_user();

		$this->current_user_roles = $current_user->roles[ 0 ];

		$this->current_username = $current_user->user_login;

		$this->locale              = ( defined( 'ICL_LANGUAGE_CODE' ) ) ? '_' . ICL_LANGUAGE_CODE : '';
		$this->option_name         = 'iwebsite_admin_menu_for_userrole' . $this->locale;
		$this->localization_domain = 'iwebsite-adminmenu';
		$menu_array                = array ();
		$counter                   = 0;
		if ( isset( $menu ) && ! empty( $menu ) ) {
			foreach ( $menu as $menu_item ) {
				$menu_item_slug  = $menu_item[ 2 ];
				$menu_item_label = strip_tags( $menu_item[ 0 ] );
				$menu_item_label = preg_replace( '/[0-9]+/', '', $menu_item_label );

				if ( $menu_item_label != '' ) {
					$menu_array[ $counter ][ 'menu_item_name' ]  = $menu_item_slug;
					$menu_array[ $counter ][ 'menu_item_label' ] = $menu_item_label;
					$menu_array[ $counter ][ 'menu_item_order' ] = $counter + 1;
					$menu_array[ $counter ][ 'menu_item_hide' ]  = false;
					$counter ++;
				}

			}
			$menu_array_serialized  = serialize( $menu_array );
			$this->default_settings = array (

				'serialized_menu_array' => $menu_array_serialized,
				'userrole'              => $this->current_user_roles,
				'username'              => 'clear_user_username'
			);

		}
		$this->settings = $this->get_options();

	}

}
if ( class_exists( 'iWebsite_Menu_For_Userroles' ) ) {
	$class = new iWebsite_Menu_For_Userroles();
}
